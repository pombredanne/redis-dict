try:
    import cPickle as pickle
except ImportError:
    import pickle
import random
import redis
import time


class RedisDict(object):

    MISSING = object()

    def __init__(self, name, connection, serializer=pickle):
        self.name = name
        self.serializer = serializer
        self._redis = connection

    def __len__(self):
        return self._redis.hlen(self.name)

    def _deserialize(self, msg):
        if msg is not None and self.serializer is not None:
            msg = self.serializer.loads(msg)
        return msg

    def _serialize(self, value):
        if self.serializer is not None:
            return self.serializer.dumps(value)
        return value

    def __contains__(self, key):
        assert isinstance(key, str)
        return self._redis.hexists(self.name, key)

    def __getitem__(self, key):
        assert isinstance(key, str)
        if key in self:
            msg = self._redis.hget(self.name, key)
            return self._deserialize(msg)
        raise KeyError(key)

    def __setitem__(self, key, msg):
        assert isinstance(key, str)
        msg_ = self._serialize(msg)
        self._redis.hset(self.name, key, msg_)
        return msg

    def __delitem__(self, key):
        assert isinstance(key, str)
        self._redis.hdel(self.name, key)

    def items(self, redis_connection=None):
        redis_connection = redis_connection or self._redis
        return [(k, self._deserialize(v)) for (k, v) in redis_connection.hgetall(self.name).items()]

    def update_value(self, key, updater, retry=0, interval=1000, default=MISSING):
        with self._redis.pipeline() as pipe:
            while retry+1:
                try:
                    pipe.watch(self.name)
                    if not pipe.hexists(self.name, key):
                        if default is RedisDict.MISSING:
                            raise KeyError('Key doesn\'t exists')
                        else:
                            old_value = default
                    else:
                        old_value = self._deserialize(pipe.hget(self.name, key))
                    new_value = updater(old_value)
                    pipe.multi()
                    if new_value is RedisDict.MISSING:
                        pipe.hdel(self.name, key)
                    else:
                        pipe.hset(self.name, key, self._serialize(new_value))
                    pipe.execute()
                except redis.WatchError:
                    if not retry:
                        raise
                    retry -= 1
                    time.sleep(random.randint(0, interval) * 0.001)
                else:
                    break

    def update(self, updater, retry=0, interval=1000, default=MISSING):
        with self._redis.pipeline() as pipe:
            while retry+1:
                try:
                    pipe.watch(self.name)
                    if self._redis.keys(self.name):
                        old_value = dict(self.items(redis_connection=pipe))
                    else:
                        old_value = default
                    new_value = updater(old_value)
                    pipe.multi()
                    if new_value is RedisDict.MISSING:
                        pipe.delete(self.name)
                    else:
                        if old_value and not old_value is self.MISSING:
                            deleted = [k for k in old_value if k not in new_value]
                            if deleted:
                                pipe.hdel(self.name, *deleted)
                        pipe.hmset(self.name, {key: self._serialize(value) for key,value in new_value.items()})
                    pipe.execute()
                except redis.WatchError:
                    if not retry:
                        raise
                    retry -= 1
                    time.sleep(random.randint(0, interval) * 0.001)
                else:
                    break

    def get(self, key, default=None):
        return self[key] if key in self else default

    def clear(self):
        keys = self._redis.hkeys(self.name)
        if keys:
            self._redis.hdel(self.name, *keys)

    def values(self):
        return [self._deserialize(v) for v in self._redis.hvals(self.name)]

    def keys(self):
        return self._redis.hkeys(self.name)
